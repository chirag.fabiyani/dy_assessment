const resolveOffenceCountByLevel = async (parent, params, { dataSources }) => {
  // Get all crimes data
  var crimesData = await dataSources.crimes

  /**
   * Group crimes data by the descriptions of the field name
   * @param {String} fieldName
   * @returns {object}
   */
  const groupedCrimesByFieldData = function (fieldName) {
    var groupedByFielDescDesc = crimesData.reduce((data, ele) => {
      (data[ele[fieldName]] = data[ele[fieldName]] || []).push(ele);
      return data;
    }, {})

    return groupedByFielDescDesc;
  }

  const getOffenceCountByDesc = function (groupedData) {
      var offenceCount = 0;
      groupedData.forEach(element => {
        offenceCount += parseInt(element.offenceCount);
      })
      otalOffenceCount = offenceCount
    return otalOffenceCount;
  }
  
  // returns the offence count by the description of particular field
  var groupedCrimesData = groupedCrimesByFieldData(params.field)[params.description] || []
  return {
    crimesByLevelDesc: groupedCrimesData,
    totalOffenceCountByDesc: getOffenceCountByDesc(groupedCrimesData)
  };
}

module.exports = resolveOffenceCountByLevel