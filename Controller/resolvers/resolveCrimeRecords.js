const resolveCrimeRecords = async (parent, params, { dataSources }) => {
  var crimesData = await dataSources.crimes

  // Filter crimes data by reported date 
  if (params.reportedDate) {
    var recordsByRepordedDate = crimesData.filter(element => element.reportedDate == params.reportedDate)
    return recordsByRepordedDate
  }
  return crimesData // Return all crimes data
}

module.exports = resolveCrimeRecords