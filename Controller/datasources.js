const csv=require('csvtojson')
const csvFilePath='./crime_record.csv'

const datasources = () => {
  return {
    crimes: csv({
      headers: ["id", "reportedDate", "suburb", "postCode", "offenceLevel1Desc", "offenceLevel2Desc", "offenceLevel3Desc", "offenceCount"]
    }).fromFile(csvFilePath)
  }
}

module.exports = datasources