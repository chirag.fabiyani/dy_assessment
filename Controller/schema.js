const { gql } = require('apollo-server')

const schema = gql(`
  scalar JSON

  type Crime {
    id: ID,
    reportedDate: String,
    suburb: String,
    postCode: String,
    offenceLevel1Desc: String,
    offenceLevel2Desc: String,
    offenceLevel3Desc: String,
    offenceCount: Int
  }

  type OffenceCount {
    crimesByLevelDesc: [Crime]
    totalOffenceCountByDesc: Int
  }

  type Query {
    crimeRecords(reportedDate: String): [Crime!]!
    offenceCountByLevelDesc(field: String!, description: String!): OffenceCount
  }
`)

module.exports = schema