const resolveCrimeRecords = require("./resolvers/resolveCrimeRecords")
const resolveOffenceCountByLevel = require("./resolvers/resolveOffenceCountByLevel")

const resolvers = {
  Query: {
    crimeRecords: async (parent, params, context) => {
      return resolveCrimeRecords(parent, params, context)
    },
    offenceCountByLevelDesc: async (parent, params, context) => {
      return resolveOffenceCountByLevel(parent, params, context)
    },
  },
};

module.exports = resolvers