const { ApolloServer } = require('apollo-server')
const schema = require('./schema')
const resolvers = require('./resolvers')
const datasources = require('./datasources')

const server = new ApolloServer({
  typeDefs: schema,
  resolvers,
  dataSources: datasources,
});

server.listen({port: 3006}).then(() => {
  console.log("Listening.........")
})