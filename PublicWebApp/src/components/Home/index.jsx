import React, { useEffect, useState } from "react";
import fetchAPIData from "./fetchAPIData";
import Accordion from "../Accordion";

const Home = () => {
  const [accordionData, setAccordionData] = useState({});
  const [groupBy, setGroupBy] = useState("suburb");
  const [openAccordion, setOpenAccordion] = useState({});
  const [apiJSON, setApiJSON] = useState([]);

  const handleAccordion = (key) => () => {
    const accordionKeys = { ...openAccordion };
    accordionKeys[key] = !accordionKeys[key];
    setOpenAccordion(accordionKeys);
  };

  const handleGroupBy = (e) => {
    setGroupBy(e.target.value);
  };

  useEffect(() => {
    //Fetching data from API
    fetchAPIData().then((result) => {
      setApiJSON(result);
    });
  }, [groupBy]);

  useEffect(() => {
    const temp = {};
    apiJSON?.crimeRecords?.forEach((record) => {
      if (!temp[record[groupBy]]) {
        temp[record[groupBy]] = [];
      }
      temp[record[groupBy]].push(record);
    });
    setAccordionData(temp);
  }, [groupBy, apiJSON]);

  return (
    <div>
      {/* Radio buttons for grouping */}
      <div className="w-[80%] flex flex-col justify-between mx-auto">
        <h1 className="flex justify-around text-xl my-4 font-semibold">
          Group By Field:
        </h1>
        <div className="w-full flex justify-around">
          {Object.keys(apiJSON?.crimeRecords?.[0] || {}).map((key) => (
            <div key={key}>
              <input
                className="cursor-pointer"
                name="groupBy"
                id={key}
                type="radio"
                onChange={handleGroupBy}
                value={key}
                checked={key === groupBy}
              />
              <label className="ml-3 cursor-pointer uppercase" htmlFor={key}>
                {key}
              </label>
            </div>
          ))}
        </div>
      </div>

      <div className="mt-20 flex items-center flex-col">
        {
          Object.entries(accordionData).map(([key, values]) => <div key={key} className="my-1 w-[80%] cursor-pointer select-none">
            {/* Accordions for csv data */}
            <Accordion
              accordionKey={key}
              accordionData={values}
              openAccordion={openAccordion}
              apiJSON={apiJSON}
              handleAccordion={handleAccordion}
            />
          </div>
        )}
      </div>
    </div>
  );
};

export default Home;
