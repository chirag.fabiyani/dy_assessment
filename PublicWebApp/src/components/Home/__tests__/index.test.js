import Home from '../index';
import {render, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { act } from 'react-dom/test-utils';

const mockData = {
  crimeRecords: [
    {
      "id": "1",
      "reportedDate": "01/07/2019",
      "suburb": "ADELAIDE",
      "postCode": "5000",
      "offenceLevel1Desc": "OFFENCES AGAINST PROPERTY",
      "offenceLevel2Desc": "FRAUD DECEPTION AND RELATED OFFENCES",
      "offenceLevel3Desc": "Obtain benefit by deception",
      "offenceCount": 1
    },
    {
      "id": "2",
      "reportedDate": "01/07/2019",
      "suburb": "ADELAIDE",
      "postCode": "5000",
      "offenceLevel1Desc": "OFFENCES AGAINST PROPERTY",
      "offenceLevel2Desc": "FRAUD DECEPTION AND RELATED OFFENCES",
      "offenceLevel3Desc": "Other fraud, deception and related offences",
      "offenceCount": 1
    },
    {
      "id": "3",
      "reportedDate": "01/07/2019",
      "suburb": "ADELAIDE",
      "postCode": "5000",
      "offenceLevel1Desc": "OFFENCES AGAINST PROPERTY",
      "offenceLevel2Desc": "PROPERTY DAMAGE AND ENVIRONMENTAL",
      "offenceLevel3Desc": "Graffiti",
      "offenceCount": 1
    },
    {
      "id": "4",
      "reportedDate": "01/07/2019",
      "suburb": "ADELAIDE",
      "postCode": "5000",
      "offenceLevel1Desc": "OFFENCES AGAINST PROPERTY",
      "offenceLevel2Desc": "PROPERTY DAMAGE AND ENVIRONMENTAL",
      "offenceLevel3Desc": "Other property damage and environmental",
      "offenceCount": 2
    },
    {
      "id": "5",
      "reportedDate": "01/07/2019",
      "suburb": "ADELAIDE",
      "postCode": "5000",
      "offenceLevel1Desc": "OFFENCES AGAINST PROPERTY",
      "offenceLevel2Desc": "THEFT AND RELATED OFFENCES",
      "offenceLevel3Desc": "Other theft",
      "offenceCount": 6
    },
    {
      "id": "6",
      "reportedDate": "01/07/2019",
      "suburb": "ADELAIDE",
      "postCode": "5000",
      "offenceLevel1Desc": "OFFENCES AGAINST PROPERTY",
      "offenceLevel2Desc": "THEFT AND RELATED OFFENCES",
      "offenceLevel3Desc": "Theft from motor vehicle",
      "offenceCount": 2
    },
    {
      "id": "7",
      "reportedDate": "01/07/2019",
      "suburb": "ADELAIDE",
      "postCode": "5000",
      "offenceLevel1Desc": "OFFENCES AGAINST PROPERTY",
      "offenceLevel2Desc": "THEFT AND RELATED OFFENCES",
      "offenceLevel3Desc": "Theft from shop",
      "offenceCount": 5
    },
    {
      "id": "8",
      "reportedDate": "01/07/2019",
      "suburb": "ADELAIDE",
      "postCode": "5000",
      "offenceLevel1Desc": "OFFENCES AGAINST THE PERSON",
      "offenceLevel2Desc": "ACTS INTENDED TO CAUSE INJURY",
      "offenceLevel3Desc": "Common Assault",
      "offenceCount": 3
    },
    {
      "id": "9",
      "reportedDate": "01/07/2019",
      "suburb": "ADELAIDE",
      "postCode": "5000",
      "offenceLevel1Desc": "OFFENCES AGAINST THE PERSON",
      "offenceLevel2Desc": "ACTS INTENDED TO CAUSE INJURY",
      "offenceLevel3Desc": "Serious Assault not resulting in injury",
      "offenceCount": 1
    },
    {
      "id": "10",
      "reportedDate": "01/07/2019",
      "suburb": "ADELAIDE AIRPORT",
      "postCode": "5950",
      "offenceLevel1Desc": "OFFENCES AGAINST PROPERTY",
      "offenceLevel2Desc": "THEFT AND RELATED OFFENCES",
      "offenceLevel3Desc": "Other theft",
      "offenceCount": 1
    }
  ]
}

jest.mock('../fetchAPIData',() => {
  return async () => {
    return mockData
  }
})

describe("Home component", () => {
  test("Component rendered successfully", async ()=>{
    await act(async () => {
      render(<Home />);
    })
    const items = await screen.findAllByText("Group By Field:")
    expect(items).toHaveLength(1)
  })

  test("Radio Buttons Switching successfully", async ()=>{
    await act(async () => {
      render(<Home />);
    })
    const RadioButton1 = await screen.getByLabelText("suburb");
    const RadioButton2 = await screen.getByLabelText("id");
    expect(RadioButton1.checked).toBeTruthy()
    expect(RadioButton2.checked).toBeFalsy()
    await act(async () => {
      userEvent.click(RadioButton2)
    })
    expect(RadioButton1.checked).toBeFalsy()
    expect(RadioButton2.checked).toBeTruthy()
  })
})