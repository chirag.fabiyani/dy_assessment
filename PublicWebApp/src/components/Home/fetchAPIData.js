const fetchAPIData = async () => {
  return fetch('http://localhost:3006/',{
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      query: `
        query($reportedDate: String) {
          crimeRecords (reportedDate: $reportedDate) {
            id
            reportedDate
            suburb
            postCode
            offenceLevel1Desc
            offenceLevel2Desc
            offenceLevel3Desc
            offenceCount
          }
        }
      `,
      variables: {}
    })
  })
  .then(res => res.json())
  .then(res => res?.data)
}

export default fetchAPIData