import React from "react";

const Grid = ({ accordionData, apiJSON }) => {
  return (
    <div className="p-2 w-full flex flex-col">
      <div className="grid grid-cols-8 border border-black border-collapse divide-x divide-black font-semibold uppercase">
        {
          Object.keys(apiJSON?.crimeRecords?.[0] || {}).map((key) => <span key={key} className="px-3 py-1">
            {key}
          </span>)
        }
      </div>
      {
        accordionData.map(({id, reportedDate, suburb, postCode, offenceLevel1Desc, offenceLevel2Desc, offenceLevel3Desc, offenceCount}) => <div key={id} className="grid grid-cols-8 border border-black border-collapse divide-x divide-black">
          <span className="px-3 py-1">{id}</span>
          <span className="px-3 py-1">{reportedDate}</span>
          <span className="px-3 py-1">{suburb}</span>
          <span className="px-3 py-1">{postCode}</span>
          <span className="px-3 py-1">{offenceLevel1Desc}</span>
          <span className="px-3 py-1">{offenceLevel2Desc}</span>
          <span className="px-3 py-1">{offenceLevel3Desc}</span>
          <span className="px-3 py-1">{offenceCount}</span>
        </div>)
      }
    </div>
  )
};

export default Grid;
