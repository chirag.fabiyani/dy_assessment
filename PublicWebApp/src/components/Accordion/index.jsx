import React from "react";
import Grid from "../Grid";

const Accordion = ({
  accordionData,
  accordionKey,
  openAccordion,
  apiJSON,
  handleAccordion,
}) => {
  return (
    <>
      <div key={accordionKey} className="my-1 cursor-pointer select-none mx-auto">
        <div
          className="flex justify-between border border-blue-400 bg-gray-100 w-full p-4"
          onClick={handleAccordion(accordionKey)}
          data-testid="click"
        >
          <span>{accordionKey}</span>
          <span
            className={`${
              openAccordion[accordionKey] ? "rotate-180" : "rotate-0"
            } transition-all duration-300`}
          >
            <svg
              width="24"
              height="24"
              xmlns="http://www.w3.org/2000/svg"
              fillRule="evenodd"
              clipRule="evenodd"
            >
              <path d="M23.245 4l-11.245 14.374-11.219-14.374-.781.619 12 15.381 12-15.391-.755-.609z" />
            </svg>
          </span>
        </div>
        <div className={`${openAccordion[accordionKey] ? 'max-h-[500px]' : 'max-h-0' } shadow-xl overflow-y-auto transition-all duration-300`}>
          {/* Grid */}
          <Grid
            accordionData={accordionData}
            apiJSON={apiJSON}
          />
        </div>
      </div>
    </>
  );
};

export default Accordion;
